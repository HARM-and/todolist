import 'package:flutter/material.dart';
import 'package:to_do_list/components/tasks/task_master.dart';
import 'package:to_do_list/components/tasks/task_details.dart';
import 'package:to_do_list/data/tasks.dart' as data;
import 'package:to_do_list/models/task.dart';

class AllTasks extends StatefulWidget {
  const AllTasks({Key? key, required String title}) : super(key: key);

  @override
  _AllTasksState createState() => _AllTasksState();



}

class _AllTasksState extends State<AllTasks> {

  Task? selected;
  
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the AllTasks object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text('To Do List'),
        ),
        body: Center(
            child: Column(
            children: [(selected != null)
                ? TaskDetails(task: selected!,)
                : Container(),

              Expanded(
                child: TasksMaster(
                  tasks: data.tasks,
                  openTask: (task){setState(() {
                    selected = task;
                  });
                  },
                ),
              )
            ]
            )
        )
    );
  }
}
