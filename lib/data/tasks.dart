import 'package:to_do_list/models/task.dart';
import 'dart:math';
import 'package:faker/faker.dart';

  var faker =  Faker();

final randomNumberGenerator = Random();

final tasks = List<Task>.generate(10,(int index) => Task(index+1,faker.lorem.sentence(),randomNumberGenerator.nextBool(), DateTime.now()));