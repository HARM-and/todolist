import 'package:flutter/material.dart';

import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/components/tasks/task_preview.dart';

class TasksMaster extends StatelessWidget
{
  const TasksMaster({required this.openTask, required this.tasks, Key? key}) : super(key: key);

  final List<Task>tasks;
  final Function openTask;

  @override
  Widget build(BuildContext context)
  {
    return ListView.separated(
        padding: const EdgeInsets.all(25),
        itemCount: tasks.length,
        itemBuilder: (BuildContext context, int index)
        => TaskPreview(
            task: tasks[index],
            openTask: (Task task)
            => openTask(task)
          ),
        separatorBuilder: (BuildContext context, int index) => const Divider()
    );

  }
}
