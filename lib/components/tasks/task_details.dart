import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/screens/one_task.dart';

class TaskDetails extends StatelessWidget {
  const TaskDetails({required this.task, Key? key}) : super(key: key);

final Task? task;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Row(
          children: [
            Column(
              children: <Widget>[
                Text(task!.content)
            ]
            )
          ],
        ),
      ),
    );
  }
}//