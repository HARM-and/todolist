import 'package:flutter/material.dart';
import 'package:to_do_list/models/task.dart';
import 'package:to_do_list/data/tasks.dart';
import 'package:to_do_list/screens/one_task.dart';

class TaskPreview extends StatelessWidget {
  const TaskPreview({required this.openTask, required this.task, Key? key}) : super(key: key);

  final Task task;
  final Function openTask;
  final Color uncheckColor = const Color(0xFFCCCCCC);
  final Color checkColor = const Color(0xFFCCFFCC);

  @override
  Widget build(BuildContext context) {
    return ListTile(

        title: Text(task.content),
        subtitle: Text(task.createdAt.toString()),
        key: key,
        tileColor: task.completed ? checkColor : uncheckColor,
        onTap: ()
        => openTask(task),
    );
  }
}